<!--
Dit bestand laat zien hoe je de jquery biblotheek kunt toevoegen.
Ook wordt een eerste begin gemaakt met het gebruik van jquery.
-->
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="../css/main.css" rel="stylesheet" type="text/css"/>
         <link href="../css/table.css" rel="stylesheet" type="text/css"/>
         <link href="css/setup.css" rel="stylesheet" type="text/css"/>
         
         <!--
         JQUERY BIBLIOTHEEK WORDT OPGEHAALD BIJ GOOGLE
         -->
         <script type="text/javascript" src="../jquery/jquery.js"></script>
         <script type="text/javascript" src="js/setup.js"></script>
    </head>
    <body>
 <div id="container">
            
         <!--
         Id clickme wordt na een click door de code in 
         het bestand clickme.js verborgen.
         -->
         <div id='clickme' class='clickme'>Klik op me!!!</div>       
         
         
            <div id="header">  
                
                <h1>Jquery setup</h1>
                
            <div id="menu">  
               

            <ul>
                <li><a href="index.php">home</a></li>
                <li><a href="http://www.wereldbolt.nl" target="blank">wereldbolt</a></li>
                <li><a href="eigenpagina.html">mijn super geheime pagina</a></li>
            </ul>

      
            </div>
                 </div>
            
            <div id="content">  
                
         
                <div id="col1">
                    <h1>Stappen</h1>
                <ol>
                    <li>voeg een link toe naar jquery</li>
                    <li>voeg een map js toe aan je <u>project</u></li>
                    <li>maak in de map een bestand: clickme.js</li>
                    <li>voeg code toe aan clickme.js</li>
                    <li>maak een div met een id 'clickme'</li>
                </ol>
                
               </div>
                
                
                <div id="col2">
                    <h1>Code</h1>
                        <?php
                        //highlights the given string.
                        
                        /**
                         * 
                         */
                        $var = trim('
                                <?php <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> ?>
                                 ');

                         highlight_string($var);
                       ?>
                </div>
            </div>
            
            <div id="footer">   </div>         
            
        </div>
    </body>
</html>
