<!--
Dit bestand laat zien hoe je de jquery biblotheek kunt toevoegen.
Ook wordt een eerste begin gemaakt met het gebruik van jquery.
-->
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <link href="../css/setup.css" rel="stylesheet" type="text/css"/>
        <link href="css/event.css" rel="stylesheet" type="text/css"/>

        <!--
        JQUERY BIBLIOTHEEK WORDT OPGEHAALD BIJ GOOGLE
        -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/basic.js"></script>
    </head>
    <body>
        <div id="container">




            <div id="header">  

                <h1>Jquery event</h1>

                <div id="menu">  


                    <ul>
                        <li><a href="index.php">home</a></li>
                        <li><a href="http://www.wereldbolt.nl" target="blank">wereldbolt</a></li>
                        <li><a href="eigenpagina.html">mijn super geheime pagina</a></li>
                    </ul>


                </div>
            </div>

            <div id="content">  

                <!--
                Op elke div kan een right click worden gedaan.
                -->
                <div id='basicclick' class='clickme'>basicclick</div>       
                <div id='paramclick' class='clickme'>click with parameter</div>       
                <div id='objectparamclick' class='clickme'>click with object as parameter</div>       

            </div>

            <div id="footer">   </div>         

        </div>
    </body>
</html>
