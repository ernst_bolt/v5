(function($) { // Hide scope, no $ conflict

    function basicClick() {
        alert('basic click');
    }
    
    function paramClick(msg) {
        alert(msg);
    }
    
    function paramObjectClick(obj) {
        alert(obj.msg);
    }

    $(document).ready(function() {
        $('#basicclick').click(function(){basicClick();});
        $('#paramclick').click(function(){paramClick('function with parameter');});
        $('#objectparamclick').click(function(){paramObjectClick({msg : 'function with object as parameter'});});
    });

})(jQuery);
