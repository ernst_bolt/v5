


/*
 * Met deze methode moet je altijd beginnen.
 * Dan weet je zeker dat jquery functies pas
 * worden gestart als de pagina helemaal geladen
 * zijn. Dit is heel belangrijk.
 */
$(document).ready(function () {

    /*Als je op id clickme klikt dan verdwijnt het html-element. 
     * Let op het meegeven van this. This verwijst naar het element
     * waarop geklikt is.*/
    $('#clickme').click(
            function(){
                $(this).hide();  
            }
    );

    /*De methode slideUp(). Zorgt ervoor dat het element naar 
     * boven wordt dicht-geschoven. 
     */
    $('#col2').click(function(){
        $(this).slideUp();
        
        /*Je kunt ook css manipuleren. In dit geval krijgt 
         * het element een oranje background-color.
         */
         $('#col1').css({'background-color':'orange'});
    });
       
});
