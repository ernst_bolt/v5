/* 
 * In dit bestand staan verschillende manieren hoe je functies in jquery kunt
 * maken.
 * 
 * Er is gebruik gemaakt van het learn center van jquery: 
 * http://learn.jquery.com/javascript-101/functions/
 */

/*
 * Methode 1
 */

function basicfunc() {
    alert('Eenvoudige methode');
}

/*
 * Methode 2: named function
 * Wellicht vind je dit een beetje vreemd, dat een functie wordt
 * toegekend aan een variable.
 * Het is een beetje een gedachtenkronkel. Maar ook weer niet heel gek
 * als je je realiseert dat: een variabele een 'bakje' is dat iets kan bevatten.
 * In dit geval bevat het bakje een function. Prima toch.
 */
var namedfunc = function() {
    alert('named function');
}

/*
 * Je kunt ook parameters meegeven aan een functie.
 * In het voorbeeld kunt je een dier meegeven en het geluid
 * dat het kan geven.
 */
var dierengeluid = function(dier, geluid) {
    var text = 'dier: ' + dier + ', maakt geluid: ' + geluid;
    return text;
};

/*
 * Het verschil met dieren geluid is dat er een 
 * functie wordt  gereturned.
 */
var zelfstandigdierengeluid = function(dier, geluid) {
    var text = 'dier: ' + dier + ', maakt geluid: ' + geluid;
    return function(){
    alert(text);
    };
};

$(document).ready(function() {

    /*Als je op id clickme klikt dan verdwijnt het html-element. 
     *In onderstaande functie wordt basisfunc()aangeroepen. 
     */
    $('#basicfunc').click(function() {
        basicfunc();
    });



    /*Als je op id clickme klikt dan verdwijnt het html-element. 
     *In onderstaande functie wordt namedfunc()aangeroepen.
     */
    $('#namedfunc').click(function() {
        namedfunc();
    });

    /*
     *Roept methode dierengeluiden aan en ontvangt text terug. 
     */
    $('#apengeluid').click(function(){
        alert(dierengeluid('aap', 'wakkuwakku'));
    });
    
    /*
     *Roept functie dierengeluiden. Deze functie roept zelf ook
     *een functie aan. 
     */
    $('#zelfstandigapengeluid').click(function(){
        var geluid = zelfstandigdierengeluid('zelfstandigeaap', 'wakkuwakku');
        geluid();
    });
    
});





