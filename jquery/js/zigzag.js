/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    
$(document).ready(function () {
    
    $('#zigzag').click(function(){
        var result = $('#col1').zigzag();
        $('#col1').text( result  );
        });
});

/*Dit is een eigen jquery functie.
     * Je begint met JQuery om aan de browser aan
     * te geven dat er gebruikt moet worden gemaakt van de
     * JQuery bibliotheek.
     * 
     * fn staat voor function, en extend staat voor een uitbreiding.
     */
    jQuery.fn.extend({
        
        /*zigzag is de naam van de functie die we straks kunnen aanroepen.
         * In de functie zigzag staan een aantal variabelen.
         * Daarna volgt een loop door alle text elementen.
         */
        zigzag: function () {
            
            var str = $(this).text();
            
            //variabelen
            var zigzagText = '';
            var toggle = true;
            
            for (var i = 0, len = str.length; i < len; i++) {
                zigzagText += (toggle) ? str[i].toUpperCase() : str[i].toLowerCase();
                toggle = (toggle) ? false : true;
                
                /*
                 * Je kunt in de console van de browser loggen. Dat is handig
                 * voor debuggen.
                 */
                console.log(toggle);
            }

        return zigzagText;
        }
    });