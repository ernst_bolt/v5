<!--
In dit bestand worden er op twee manieren classes geinstantieerd
(het maken van objecten).
In de eerste plaats wordt er uit de map ClassLibLetters een class opgehaald
aan de hand van de map en de namespace.
Vervolgens is het mogelijk om met behulp van de alias een class te maken.
dit gebeurt in het voorbeeld met:
$letters = new B();

De andere methode is om in hetzelfde bestand een class te maken. 
In dit geval een class Vehicle. 
Vervolgens is het mogelijk om direct een Vehicle object te maken:
$v = new Vehicle();
Deze methode wordt AFGERADEN. De reden is dat de code slecht herbruikbaar is
en je de kans loopt dat de code onoverzichtelijk wordt.
Het is beter om in bibliotheken te werken. Daar gebeurt het zware werk,
vervolgens is het een kwestie van de juiste bibliotheken te importeren.
-->


<!DOCTYPE html>
<html>



    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <?php
       
        use ClassLibLetters\B as B;
        
		function __autoload($class) {
		// convert namespace to full file path
        // convention: namespace name is equal to folder name
		$class = str_replace('\\', '/', $class) . '.php';
		require_once($class);
}
       
        $letters = new B();
        
        //SuperBroem::drive();
        $v = new Vehicle;
		$v->drive();
        
        class Vehicle{
            function drive(){
                echo 'pruttel pruttel';
            }
        }
                
        ?>
    </body>
</html>
