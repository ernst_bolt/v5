<?php

/*
 * $_GET is niet een erg veilige manier om variabelen mee te geven.
 * 1) De waarden zijn zichtbaar in de url, kunnen gecached worden 
 * door de browser.
 * 2) De waarden kunnen veranderd worden in de url en dan worden verzonden.
 * 
 * Een alternatief is $_POST. Dit wordt vaak gebruikt om gegevens van een 
 * formulier te verzenden.
 * 
 * Hieronder een inlogformulier welke een inlognaam en een wachtwoord verstuurd.
 * De inlognaam en wachtwoord worden vervolgens boven in het scherm van dezelfde
 * pagina getoond.
 * Je kunt ook een andere pagina tonen door Header(Location: ....) te gebruiken.
 * Zie voor dit gebruik: get.php
 * 
 */

if(count($_POST) > 0){
    foreach($_POST as $key => $value){
        echo 'key: '.$key . ' value: ' . $value . '<BR>'; 
    }
}

?>

<!DOCTYPE HTML>
<head>
	<title>Homepage</title>
	<link href="inloggen.css" rel="stylesheet" type="text/css"/> 
	 <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
         <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</head>
<body>
	<div id="container">
		<div id="header"></div>
		<div id="content">
                    <table>
                        <!-- Je kunt als action value ook bijvorbeeld post.php, of een 
                        andere pagina gebruiken.-->
                    <form action="<?=$_SERVER['PHP_SELF']?>" method="post">
                        <tr><td>Gebruikersnaam:</td><td> <input type="text" name="naam" /></td></tr>
                        <tr><td>Wachtwoord: </td><td><input type="password" name="wachtwoord" /></td></tr>
                        <tr><td><input type="submit" value="Inloggen" /></td></tr>
                    </form>
                    </table>
                    
		</div>
		<div id="footer"></div>
	</div>
</body>
