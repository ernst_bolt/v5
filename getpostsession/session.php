<?php
session_start();

/*
 * Gegevens in $_POST worden verzonden en zijn in de landingspagina beschikbaar.
 * De gegevens worden vervolgens uit $_POST verwijderd. Dit hoef je niet handmatig
 * te doen.
 * 
 * Je kunt gegevens dus met $_POST niet meegeven aan meerdere pagina's. Dit kan 
 * wel door gegevens in $_SESSION te plaatsen.
 * 
 * Pas als de session niet vervolgt wordt, vernietigd of de gegevens uit de 
 * session worden verwijderd, zijn de gegevens niet meer beschikbaar.
 * 
 * De naam van session_start() is iets misleidend. Het is namelijk niet alleen
 * het starten van een session maar ook het vervolgen van een session.
 * Dit betekent dat in elke pagina die gebruik wil maken van een bestaande
 * session er gestart moet worden met session_start(). Samengevat:
 * session_start() wordt ook gebruikt om een session te vervolgen (resume).
 */

$_SESSION['sessionvalue'] = 'Ik ben de grote session.';
echo 'Deze code is uitgevoerd:' . '<BR>';
$stringtoformat = "<?php \$_SESSION['sessionvalue'] = 'Ik ben de grote session.'; ?>";
highlight_string($stringtoformat);
?>
