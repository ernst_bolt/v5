<?php

/*
 * Je kunt aan de url gegevens meegeven die je kunt uitlezen
 * in een array: $_GET.
 * 
 * Het is belangrijk om een controle te doen of de key bestaat.
 * $_GET noemen we een global. Dit is een standaard array die PHP 
 * zelf aanmaakt.
 * 
 * De onderstaande url geeft de key: getnaam en value: ernst mee.
 * http://localhost...getpostsession/get.php/?getnaam=ernst 
 */

if(isset($_GET['getnaam'])){
    echo 'getnaam: ' . $_GET['getnaam'] . '<BR>';
} else{
    Header("Location: landing.php?getnaam=no \$_GET['getnaam'] value set in get.php ");
}

?>
