<html>
   <!--http://www.w3schools.com/php/php_ajax_database.asp-->
   
<head>
<script>
    /*
     * Deze functie maakt een object aan. => new XMLHttpRequest()... of new ActiveObject(...
     * Het object wordt verzonden. => xmlhttp.open(... en xmlhttp.send(...
     * De server krijgt de aanvraag met het object binnen. Zie het bestand ajaxresponse.php
     * De server verwerkt de aanvraag en geeft de response terug.
     * Javascript verwerkt vervolgens de response in de bestaande pagina. =>
     * xmlhttp.onreadystatechange=function(){...
     * 
     * Happy Ajaxing!
     */
function showUser(str)
{
if (str=="")
  {
  document.getElementById("txtHint").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","ajaxresponse.php?q="+str,true);
xmlhttp.send();
}
</script>
</head>
<body>

<form>
<select name="users" onchange="showUser(this.value)">
<option value="">Select a person:</option>
<option value="1">Peter Griffin</option>
<option value="2">Lois Griffin</option>
<option value="3">Glenn Quagmire</option>
<option value="4">Joseph Swanson</option>
</select>
</form>
<br>
<div id="txtHint"><b>Person info will be listed here.</b></div>

</body>
</html> 
