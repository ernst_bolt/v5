<?php
//http://www.w3schools.com/php/php_ajax_database.asp


/*
 * De array $users bevat alle users die geretourneerd kunnen worden.
 * Dit in plaats van een database.
 * Daar komen we nog over te spreken verderop in het jaar.
 */
$users = array(
    1 =>   array(
                    'Firstname'     => 'Peter',
                    'Lastname'      => 'Griffin',
                    'Age'           => 41,
                    'Hometown'      => 'Quahog'
                ),
    2 =>   array(
                    'Firstname'     => 'Lois',
                    'Lastname'      => 'Griffin',
                    'Age'           => 36,
                    'Hometown'      => 'Newport'
                ),
    3 =>   array(
                    'Firstname'     => 'Joseph',
                    'Lastname'      => 'Swanson',
                    'Age'           => 32,
                    'Hometown'      => 'Apeldoorn'
                ),
    4 =>   array(
                    'Firstname'     => 'Glenn',
                    'Lastname'      => 'Quagmire',
                    'Age'           => 41,
                    'Hometown'      => 'Amersfoort'
                )
);

/*
 * De functie intval maakt van de waarde aan de rechterkant van de vergelijking
 * een int.
 */
$q = intval($_GET['q']);

/*
 * Controle of de waarde wel bestaat.
 */
if (!isset($users[$q]))
  {
  die('Could not find: user with id: ' . $q);
  }


/*
 * $result krijgt de array uit de array met users.
 */
$result = $users[$q];


/*
 * Vervolgens wordt de response opgebouwd: een tabel met daarin een regel
 * met de user.
 */
echo "<table border='1'>
<tr>
<th>Firstname</th>
<th>Lastname</th>
<th>Age</th>
<th>Hometown</th>
</tr>";


  echo "<tr>";
  echo "<td>" . $result['Firstname'] . "</td>";
  echo "<td>" . $result['Lastname'] . "</td>";
  echo "<td>" . $result['Age'] . "</td>";
  echo "<td>" . $result['Hometown'] . "</td>";
  echo "</tr>";

  echo "</table>";

?> 