<?php

//http://www.w3schools.com/php/php_ajax_database.asp


$arr =
        array('a' =>
            array(
                'man' => 'Ernst',
                'age' => 34,
                'children' => array('Lucas', 'Carmen', 'Quinten', 'Fleur'),
                'dog' => array('name' => 'Sep', 'age' => 1.5)
            ),
            'c' => array(
                'woman' => 'Jorine',
                'age' => 34
            )
            ,
            'b' =>
            array(
                'hairman' => 'Hobbit',
                'age' => 340,
                'children' => array('Gollum'),
                'dog' => array('name' => 'Messi', 'age' => 1.5)
            ),
            'd' => array(
                'chairman' => 'Gandalf',
                'age' => 3400
            ),
            array()
            )
        
;
echo json_encode(array($arr));
?> 
