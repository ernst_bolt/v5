<html>
    <!-- In dit voorbeeld wordt het ajax request verzonden via de methode POST.
    Het verschil met GET is dat er data meegegeven kan worden (zonder dat dit
    versleuteld zit in de URL).
    Ook wordt de data niet gecached. Wat bij beveiligde data noodzakelijk is.
    
    http://api.jquery.com/jQuery.post/
    Jquery post in de beschrijving:
    jQuery.post( url [, data ] [, success(data, textStatus, jqXHR) ] [, dataType ] )
    
    -->

    <head>

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.js"></script>

        <script>


            /*
             * Verberg de tabel die het ajax resultaat laat zien
             */
            $(document).ready(function() {
                $('#txtHint').hide();

                $('#users').change(function() {
                    
                    //laat popup box zien met dropdown waarde
                    alert($('select').val());
                    
                    //voer de functie showUser() uit, en geef de
                    //waarde van de dropdown mee.
                    
                    showUser($('select').val());
                });

            });

            /*
             * Deze functie maakt gebruik van jquery, ajax en json.
             * De magic van jquery is duidelijk zichtbaar.
             * Wat met javascript allemaal handmatig moet gebeuren kun je met
             * jquery eenvoudig doen met $.getJSON(url....
             * 
             * 
             * Happy JQaxing!
             */
            function showUser(str)
            {
                /*
                 * Er wordt een standaard instelling van ajax gewijzigd.
                 * In de functie wordt een request gedaan voor data, maar
                 * onderstussen dendert de functie gewoon verder.
                 * Dat willen we niet, want anders wordt de tabel getoond,
                 * zonder de juiste gegevens.
                 * Als de gegevens ontvangen zijn dan zien we de gegevens
                 * in de tabel gewijzigd worden.
                 * Daarom wordt de instelling gewijzigd, zodat er niet gelijktijdig
                 * meerdere processen kunnen lopen.
                 */
                $.ajaxSetup({'async': false});


                /*
                 Met $.post doe je een request waarbij je via POST data kunt
                 meegeven. 
                 De url heeft nu geen parameters meer in zich.
                 De data parameter van $.post geeft het datatype weer dat verwacht
                 wordt van de server.
                 Let op: de data heeft de vorm van een json object. Een name
                 value combinatie is verplicht.
                 */
                $.post("ajaxmetjsonresponsepost.php", {"userid" : str}, function(data) {
                    console.log(data);
                    $('#Firstname').text(data.Firstname);
                    $('#Lastname').text(data.Lastname);
                    $('#Age').text(data.Age);
                    $('#Hometown').text(data.Hometown);
                }, 'json');

                $('#txtHint').show();
            }
        </script>
    </head>
    <body>

        <form>
            <select id="users">
                    <option value="">Select a person:</option>
                <option value="1">Peter Griffin</option>
                <option value="2">Lois Griffin</option>
                <option value="3">Glenn Quagmire</option>
                <option value="4">Joseph Swanson</option>
            </select>
        </form>
        <br>

        <!-- Deze div wordt verborgen door jquery. Zie het script bovenaan de pagina.
        Na een keuze in de dropdown wordt er een user opgehaald, de tabel gevuld 
        en de tabel zichtbaar gemaakt -->
        <div id="txtHint">
            <table border='1'>
                <tr><th>Firstname</th><th>Lastname</th><th>Age</th><th>Hometown</th></tr>
                <td id="Firstname">Will be changed!</td>
                <td id="Lastname">Will be changed!</td>
                <td id="Age">Will be changed!</td>
                <td id="Hometown">Will be changed!</td>
            </table></div>

    </body>
</html> 



