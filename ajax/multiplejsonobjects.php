<?php

/*
 * Er wordt een ajax request uitgevoerd, dat een object teruggeeft met meerdere geneste objecten.
 */


?>
<html>

    <head>

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.js"></script>

        <script>


            /*
             * Er worden met behulp van ajax meerdere json objecten verkregen.
             */
            $(document).ready(function() {
                getJsonObjects();
            });

            
            function getJsonObjects()
            {
                $.ajaxSetup({'async': false});

                /*
                 Er wordt aangegeven hoeveel json objecten er teruggegeven moeten worden.
                 */
                $.post("multiplejsonobjectsresponse.php", {}, function(data) {
                    console.log(data);
                }, 'json');
            }
        </script>
    </head>
    <body>
    </body>
</html> 



