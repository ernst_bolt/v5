<?php
//http://www.w3schools.com/php/php_ajax_database.asp


/*
 * De array $users bevat alle users die geretourneerd kunnen worden.
 * Dit in plaats van een database.
 * Daar komen we nog over te spreken verderop in het jaar.
 */
$users = array(
    1 =>   array(
                    'Firstname'     => 'Peter',
                    'Lastname'      => 'Griffin',
                    'Age'           => 41,
                    'Hometown'      => 'Quahog'
                ),
    2 =>   array(
                    'Firstname'     => 'Lois',
                    'Lastname'      => 'Griffin',
                    'Age'           => 36,
                    'Hometown'      => 'Newport'
                ),
    3 =>   array(
                    'Firstname'     => 'Joseph',
                    'Lastname'      => 'Swanson',
                    'Age'           => 32,
                    'Hometown'      => 'Apeldoorn'
                ),
    4 =>   array(
                    'Firstname'     => 'Glenn',
                    'Lastname'      => 'Quagmire',
                    'Age'           => 41,
                    'Hometown'      => 'Amersfoort'
                )
);

/*
 * De functie intval maakt van de waarde aan de rechterkant van de vergelijking
 * een int.
 */
$q = intval($_GET['q']);

/*
 * Controle of de waarde wel bestaat.
 */
if (!isset($users[$q]))
  {
  die('Could not find: user with id: ' . $q);
  }

/*
 * $result krijgt de array uit de array met users.
 * Die krijgt het jsonformat via json_encode
 */
$result = $users[$q];
echo json_encode($result);
?> 
