<html>
   <!--http://www.w3schools.com/php/php_ajax_database.asp-->
   
<head>
<script>
    
    
    /*
     * Verberg de tabel die het ajax resultaat laat zien
     */
    window.onload = hideTxtHint; 
    
    function hideTxtHint(){
    document.getElementById("txtHint").style.display='none';
    }
    
    function showTxtHint(){
    document.getElementById("txtHint").style.display='block';
    }
    
    /*
     * Deze functie maakt een object aan. => new XMLHttpRequest()... of new ActiveObject(...
     * Het object wordt verzonden. => xmlhttp.open(... en xmlhttp.send(...
     * De server krijgt de aanvraag met het object binnen. Zie het bestand ajaxresponse.php
     * De server verwerkt de aanvraag en geeft de response terug.
     * Javascript verwerkt vervolgens de response in de bestaande pagina. =>
     * xmlhttp.onreadystatechange=function(){...
     * 
     * Happy Ajaxing!
     */
function showUser(str)
{
if (str=="")
  {
  document.getElementById("txtHint").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
        //Maakt van de responsetext een json object.
        //Zie voor meer informatie over json-objecten:
        //http://www.w3schools.com/json/json_intro.asp
        //Erg makkelijk om te begrijpen!
        var jsonObj = JSON.parse(xmlhttp.responseText);
    document.getElementById("Firstname").innerHTML=jsonObj.Firstname;
    document.getElementById("Lastname").innerHTML=jsonObj.Lastname;
    document.getElementById("Age").innerHTML=jsonObj.Age;
    document.getElementById("Hometown").innerHTML=jsonObj.Hometown;
    showTxtHint();
    }
  }
xmlhttp.open("GET","ajaxmetjsonresponse.php?q="+str,true);
xmlhttp.send();
}
</script>
</head>
<body>

<form>
<select name="users" onchange="showUser(this.value)">
<option value="">Select a person:</option>
<option value="1">Peter Griffin</option>
<option value="2">Lois Griffin</option>
<option value="3">Glenn Quagmire</option>
<option value="4">Joseph Swanson</option>
</select>
</form>
<br>

<!-- Deze div wordt verborgen door javascript. Zie het script bovenaan de pagina.
Na een keuze in de dropdown wordt er een user opgehaald, de tabel gevuld 
en de tabel zichtbaar gemaakt -->
<div id="txtHint">
<table border='1'>
    <tr><th>Firstname</th><th>Lastname</th><th>Age</th><th>Hometown</th></tr>
    <td id="Firstname">Will be changed!</td>
    <td id="Lastname">Will be changed!</td>
    <td id="Age">Will be changed!</td>
    <td id="Hometown">Will be changed!</td>
</table></div>

</body>
</html> 



