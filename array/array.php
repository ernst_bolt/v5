<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        
<?php
function format_highlight_string($stringtoformat){
    $s = '<?php ' . $stringtoformat . '?>';
    echo '<BR>';
    highlight_string($s);
    echo '<BR>';
}
?>

        <?php

echo '<b>EENDIMENSIONALE ARRAYS</b>';        
echo "<BR><BR>";


//*********************************************************
echo "ARRAY INSTANTIE";
format_highlight_string(
'
$array = array();   //instantie
');

$array = array();   //instantie
var_dump($array);   //result
echo "<BR><BR>";



//*********************************************************
echo "ARRAY ELEMENTEN PUSHEN";
format_highlight_string(
        '
//methode 1
$array[0] = "greijdanus"; //push
$array[1] = "windesheim"; //push

//methode 2
array_push($array, "superschool"); //push
        ');


$array = array();   //instantie

//methode 1
$array[0] = "greijdanus"; //push
$array[1] = "windesheim"; //push

//methode 2
array_push($array, "superschool"); //push


var_dump($array);   //result
echo "<BR><BR>";



//*********************************************************
echo "ARRAY ELEMENTEN POPPEN";
format_highlight_string(
        '
            //voor de pop
            $array[0] = "greijdanus"; //push
            $array[1] = "windesheim"; //push
            $array[2] = "superschool"; //push

            //pop
            array_pop($array);
        ');


$array = array();   //instantie

//voor de pop
$array[0] = "greijdanus"; //push
$array[1] = "windesheim"; //push
$array[2] = "superschool"; //push

//pop
array_pop($array);

//na de pop
var_dump($array);   //result
echo "<BR><BR>";

//*********************************************************
echo "ARRAY KEYS";
format_highlight_string(
        '
        //elementen
        $array[0] = "greijdanus"; //push
        $array[1] = "windesheim"; //push
        $array[2] = "superschool"; //push

        //keys
        $keys = array_keys($array);
        ');


$array = array();   //instantie

//elementen
$array[0] = "greijdanus"; //push
$array[1] = "windesheim"; //push
$array[2] = "superschool"; //push

//keys
$keys = array_keys($array);

//resultaat
var_dump($keys);   //result
echo "<BR><BR>";

//*********************************************************
echo "ARRAY KEYS MET ASSOCIATIES";
format_highlight_string(
        '
        //elementen
        $array["vo"] = "greijdanus"; //push
        $array["hbo"] = "windesheim"; //push
        $array["alles"] = "superschool"; //push

        //keys
        $keys = array_keys($array);
        ');


$array = array();   //instantie

//elementen
$array["vo"] = "greijdanus"; //push
$array["hbo"] = "windesheim"; //push
$array["alles"] = "superschool"; //push

//keys
$keys = array_keys($array);

//resultaat
var_dump($keys);   //result
//echo $keys[0];
echo "<BR><BR>";

//*********************************************************
echo "CONTROLE BESTAAN VAN KEY IN ARRAY";
format_highlight_string(
        '
//elementen
$array["vo"] = "greijdanus"; //push
$array["hbo"] = "windesheim"; //push
$array["alles"] = "superschool"; //push

//resultaat
var_dump(key_exists("conservatorium", $array));
        ');


$array = array();   //instantie

//elementen
$array["vo"] = "greijdanus"; //push
$array["hbo"] = "windesheim"; //push
$array["alles"] = "superschool"; //push

//resultaat
var_dump(key_exists("conservatorium", $array));
echo "<BR><BR>";

//*********************************************************
echo "ARRAY ASSOCIATIES INSTANTIE IN EEN KEER";
format_highlight_string(
        '
        $array = array(
                "vo" => "greijdanus",
                "hbo" => "windesheim",
                "alles" => "superschool"    
        );   //instantie

        //keys
        $keys = array_keys($array);

        //resultaat
        var_dump($keys);   //result keys
        var_dump($array);   //result array

        //LET OP: DIT KAN NIET:
        echo $array[0];
        ');


$array = array(
        "vo" => "greijdanus",
        "hbo" => "windesheim",
        "alles" => "superschool"    
);   //instantie

//keys
$keys = array_keys($array);

//resultaat
var_dump($keys);   //result keys
var_dump($array);   //result array
echo "<BR><BR>";

//*********************************************************
echo "COUNT ELEMENTEN IN ARRAY";
format_highlight_string(
        '
echo count($array);
        ');


$array = array(
        "vo" => "greijdanus",
        "hbo" => "windesheim",
        "alles" => "superschool"    
);   //instantie

//count
echo count($array);
echo empty($array);
echo "<BR><BR>";

//*********************************************************
echo "LOOP DOOR ELEMENTEN IN ASSOCIATIEVE ARRAY";
format_highlight_string(
        '
        $array = array(
                "vo" => "greijdanus",
                "hbo" => "windesheim",
                "alles" => "superschool"    
        );   //instantie

        //loop
        foreach($array as $key => $value){
            echo "| key: $key , value: $value ";
        }
        ');


$array = array(
        "vo" => "greijdanus",
        "hbo" => "windesheim",
        "alles" => "superschool"    
);   //instantie

//loop
foreach($array as $key => $value){
    echo "| key: $key , value: $value ";
}
echo "<BR><BR>";

//*********************************************************
echo "LOOP DOOR ELEMENTEN IN NIET-ASSOCIATIEVE ARRAY";
format_highlight_string(
        '
        $array = array(
                "greijdanus",
                "windesheim",
                "superschool"    
        );   //instantie

        //loop
        foreach($array as $value){
            echo "| value: $value ";
        }
        ');


$array = array(
                "greijdanus",
                "windesheim",
                "superschool"    
        );   //instantie

        //loop
        foreach($array as $value){
            echo "| value: $value ";
        }

echo "<BR><BR>";

//*********************************************************
echo "MOVE CURSOR TO NEXT VALUE IN ARRAY";
format_highlight_string(
        '
        $array = array(
                "greijdanus",
                "windesheim",
                "superschool"    
        );   //instantie

        //loop
        for($i = 3 ; $i > 0 ; $i--){
            var_dump(next($array));
        }
        ');


$array = array(
                "greijdanus",
                "windesheim",
                "superschool"    
        );   //instantie

        //loop
        for($i = 3 ; $i > 0 ; $i--){
            var_dump(next($array));
        }

echo "<BR><BR>";

//*********************************************************
echo "MOVE CURSOR TO LAST VALUE IN ARRAY";
format_highlight_string(
        '
        $array = array(
                "greijdanus",
                "windesheim",
                "superschool"    
        );   //instantie

        //loop
        for($i = 3 ; $i > 0 ; $i--){
            var_dump(end($array));
        }
        
        //eerste element: reset($array)
        //huidige element: current($array)
        ');


$array = array(
                "greijdanus",
                "windesheim",
                "superschool"    
        );   //instantie

        //loop
        for($i = 3 ; $i > 0 ; $i--){
            var_dump(end($array));
        }

echo "<BR><BR>";
?>
    </body>
</html>
