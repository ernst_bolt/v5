<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        
<?php
function format_highlight_string($stringtoformat){
    $s = '<?php ' . $stringtoformat . '?>';
    echo '<BR>';
    highlight_string($s);
    echo '<BR>';
}

echo '<b>MULTIDIMENSIONALE ARRAYS</b>';        
echo "<BR><BR>";


//*********************************************************
echo "ARRAY EENDIMENSIONAAL (herhaling)";
format_highlight_string(
'
$array = array();   //instantie
array_push($array, "a");
array_push($array, "b");
array_push($array, "c");
');

$array = array();   //instantie
array_push($array, "a");
array_push($array, "b");
array_push($array, "c");

var_dump($array);   //result
echo "<BR><BR>";

//*********************************************************
echo "ARRAY MULTIDIMENSIONAAL";
format_highlight_string(
'
$array = array(
    "MP3" => array(
        "Mark Knopfler" => "Brothers in Arms" ,
        "Dire Straits" => "The Wall", 
        "Pink Floyd" => "Dark side of the moon" 
    ),
    "Boeken" => array(
        "God" => "Bijbel",
        "Bill Waterson" => "Casper en Hobbes",
        "Gang of four" => "Design Patterns"
    )
    
);

echo $array["MP3"]["Dire Straits"];   //result
');

$array = array(
    "MP3" => array(
        "Mark Knopfler" => "Brothers in Arms" ,
        "Dire Straits" => "The Wall", 
        "Pink Floyd" => "Dark side of the moon" 
    ),
    "Boeken" => array(
        "God" => "Bijbel",
        "Bill Waterson" => "Casper en Hobbes",
        "Gang of four" => "Design Patterns"
    )
    
);

echo "<BR>";
echo $array["MP3"]["Dire Straits"];   //result
echo "<BR><BR>";

//*********************************************************
echo "ARRAY MULTIDIMENSIONAAL - CONTROLE OP BESTAAN KEY";
format_highlight_string(
'
$array = array(
    "MP3" => array(
        "Mark Knopfler" => "Brothers in Arms" ,
        "Dire Straits" => "The Wall", 
        "Pink Floyd" => "Dark side of the moon" 
    ),
    "Boeken" => array(
        "God" => "bijbel",
        "Bill Waterson" => "Casper en Hobbes",
        "Gang of four" => "Design Patterns"
    )
    
);

var_dump(isset($array["MP3"]));   //result
var_dump(isset($array["CDs"]));   //result
');

$array = array(
    "MP3" => array(
        "Mark Knopfler" => "Brothers in Arms" ,
        "Dire Straits" => "The Wall", 
        "Pink Floyd" => "Dark side of the moon" 
    ),
    "Boeken" => array(
        "God" => "bijbel",
        "Bill Waterson" => "Casper en Hobbes",
        "Gang of four" => "Design Patterns"
    )
    
);

var_dump(isset($array["MP3"]));   //result
var_dump(isset($array["CDs"]));   //result

echo 'fout : ';format_highlight_string('isset($array["CDs"]), resultaat => '); 
$b =  isset($array["CDs"]); 
echo 'echos: ' . $b . '<BR>';
var_dump($b);

echo 'goed : ';format_highlight_string('(int)isset($array["CDs"]), resultaat =>'); 
$b =  ((int)isset($array["CDs"])); 
echo 'echos: ' . $b . '<BR>';
var_dump($b);
echo "<BR><BR>";

//*********************************************************
echo "RELATIE TUSSEN MULTIDIMENSIONALE ARRAY EN TABELLEN";
format_highlight_string(
'
$array = array(
    "MP3" => array(
        "Mark Knopfler" => "Brothers in Arms" ,
        "Dire Straits" => "The Wall", 
        "Pink Floyd" => "Dark side of the moon" 
    ),
    "Boeken" => array(
        "God" => "bijbel",
        "Bill Waterson" => "Casper en Hobbes",
        "Gang of four" => "Design Patterns"
    )
    
);


//start table
echo "<table cellspacing=0 border=1>";
foreach($array as $row => $columns){
    echo "<tr>"; //start row
    
    echo "<td>"; echo $row; echo "</td>"; //row title
    
    foreach($columns as $key => $columnvalue){
        echo "<td>"; echo $columnvalue; echo "</td>"; //row title
    }
    echo "</tr>";
}
');

$array = array(
    "MP3" => array(
        "Mark Knopfler" => "Brothers in Arms" ,
        "Dire Straits" => "The Wall", 
        "Pink Floyd" => "Dark side of the moon" 
    ),
    "Boeken" => array(
        "God" => "bijbel",
        "Bill Waterson" => "Casper en Hobbes",
        "Gang of four" => "Design Patterns"
    )
    
);


//start table
echo "<table cellspacing='0' border='1'>";
foreach($array as $row => $columns){
    echo "<tr>"; //start row
    
    echo "<td>"; echo $row; echo "</td>"; //row title
    
    foreach($columns as $key => $columnvalue){
        echo "<td>"; echo $columnvalue; echo "</td>"; //row title
    }
    echo "</tr>";
}
echo "</table>";
echo "<BR><BR>";

//*********************************************************
echo "GEMIXTE NOTATIE HTML EN PHP TEN BEHOEVE VAN TABELLEN";
format_highlight_string(
'
$array = array(
    "MP3" => array(
        "Mark Knopfler" => "Brothers in Arms" ,
        "Dire Straits" => "The Wall", 
        "Pink Floyd" => "Dark side of the moon" 
    ),
    "Boeken" => array(
        "God" => "bijbel",
        "Bill Waterson" => "Casper en Hobbes",
        "Gang of four" => "Design Patterns"
    )
    
);
?>

<!--start table -->
<table cellspacing=0 border=1>
<?php foreach($array as $row => $columns): ?>
    <tr> <!--start row-->
    
    <td> <?php echo $row; ?> </td>  <!--row title--> 
    
    <?php foreach($columns as $key => $columnvalue):?>
        <td> <?php echo $columnvalue;?> </td>
    <?php endforeach;?>
    
    </tr>
    
<?php endforeach;?>
</table>;
');

$array = array(
    "MP3" => array(
        "Mark Knopfler" => "Brothers in Arms" ,
        "Dire Straits" => "The Wall", 
        "Pink Floyd" => "Dark side of the moon" 
    ),
    "Boeken" => array(
        "God" => "bijbel",
        "Bill Waterson" => "Casper en Hobbes",
        "Gang of four" => "Design Patterns"
    )
    
);
?>

<!--start table -->
<table cellspacing='0' border='1'>
<?php foreach($array as $row => $columns): ?>
    <tr> <!--start row-->
    
    <td> <?php echo $row; ?> </td>  <!--row title--> 
    
    <?php foreach($columns as $key => $columnvalue):?>
        <td> <?php echo $columnvalue;?> </td>
    <?php endforeach;?>
    
    </tr>
    
<?php endforeach;?>
    
</table>
<BR><BR>

