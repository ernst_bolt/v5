<?php

/*
 * In deze file wordt gebruik gemaakt van mysqli.
 * 
 */

/*
 * Connectie string.
 * @param locatie, gebruikersnaam, wachtwoord, databasenaam
 * 
 */
$link = mysqli_connect("localhost", "root", "welkom", "firstchoice");

/* check connection */
$err = mysqli_connect_errno();
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

/* Create table doesn't return a resultset */
//if (mysqli_query($link, "CREATE TEMPORARY TABLE myCity LIKE City") === TRUE) {
//    printf("Table myCity successfully created.\n");
//}

/* Select queries return a resultset */
if ($result = mysqli_query($link, "SELECT * FROM winkel LIMIT 10")) {
    printf("Select returned %d rows.\n", mysqli_num_rows($result));
    while (list($colnaam1, $colnaam2) = mysqli_fetch_row($result)) {
        print("$colnaam1 $colnaam2 <br />");
    }
    /* free result set */
    mysqli_free_result($result);
}


//result as array
if ($result = mysqli_query($link, "SELECT * FROM winkel LIMIT 10")) {
    printf("Select returned %d rows.\n", mysqli_num_rows($result));
    while ($data = mysqli_fetch_row($result)) {
        var_dump($data);
    }
    /* free result set */
    mysqli_free_result($result);
}

//result as array
if ($result = mysqli_query($link, "SELECT * FROM winkel LIMIT 10")) {
    printf("Select returned %d rows.\n", mysqli_num_rows($result));
    while ($data = mysqli_fetch_assoc($result)) {
        var_dump($data);
    }
    /* free result set */
    mysqli_free_result($result);
}

mysqli_close($link);
?>

